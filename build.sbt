name := "fpinscala"

version := "1.0"

scalaVersion := "2.10.2"

resolvers ++= Seq(
  "spray repo" at "http://repo.spray.io/"
)


libraryDependencies ++= Seq(
	"org.scalatest"  %  "scalatest_2.10"%  "1.9.2"  %  "test",
    "org.hsqldb"  %  "hsqldb"  %  "2.0.0",
    "com.h2database"  %  "h2"  % "1.3.166",
    "com.typesafe.slick"  %%  "slick"  % "1.0.1",
    "com.typesafe" %% "scalalogging-slf4j" % "1.0.1")


libraryDependencies ++= Seq(
  "io.spray"            %   "spray-can"     % "1.2-M8",
  "io.spray"            %   "spray-routing" % "1.2-M8",
  "io.spray"            %   "spray-testkit" % "1.2-M8",
  "io.spray"          %%  "spray-json"        % "1.2.5",
   "net.liftweb" %% "lift-json" % "2.5.1",
  "com.typesafe.akka"   %%  "akka-actor"    % "2.2.0-RC1",
  "com.typesafe.akka"   %%  "akka-testkit"  % "2.1.4",
  "org.specs2"          %%  "specs2"        % "1.14" % "test"
)

