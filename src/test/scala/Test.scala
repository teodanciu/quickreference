import org.scalatest.FunSuite
import org.scalatest.matchers.ShouldMatchers

class Test extends FunSuite with ShouldMatchers {

  test("Regex") {
    val regex = "^(\\d+)([a-zA-Z]+)".r

    val input = "12345ABC"

    val result = input match {
      case regex(digits, letters) => Some((digits, letters))
      case _ => None
    }

    val Some((digits, letters)) = result

    digits should be === "12345"
    letters should equal("ABC")
  }

  test("Collections") {
    val map = Map(1 -> "one")
    val list = List()
    map should not be 'empty
    list should be('empty)
  }

  test("Expect Exception") {
    evaluating {
      "hello".charAt(-1)
    } should produce[IndexOutOfBoundsException]
  }


}
