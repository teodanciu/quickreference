package io

import java.lang.Exception
import util.control.Exception._

object InputManager {

  def readProgramInput(): ProgramInput =
    readUntilYes(new ProgramInput)(())

  def readConfirmationInput(): Input =
    readUntilYes(EmptyInput)(())

  private def readUntilYes[T <: Input](toInput: => T)(otherwise: => Unit): T = {
    Iterator.continually {
      val input = toInput
      println(s"\nConfirm parameters: \n\n$input")
      readLine("[Y]es? ") match {
        case "Y" => Some(input)
        case _ =>
          otherwise
          None
      }
    }.find(_.isDefined).get.get
  }

}


trait Input {

  protected def readUntilRight[T](prompt: String, default: String)(op: String => T): T = {
    def readUntilRight0(again: Boolean = false): T = {
          if (again) println("Incorrect format.  Try again.")
          catching(classOf[Exception]).opt(op(readWithDefault(prompt, default))) match {
            case Some(date) => date
            case None => readUntilRight0(true)
          }
        }
        readUntilRight0(false)
      }

  protected def readWithDefault(prompt: String, default: String): String = {
    readLine(s"$prompt $default  : ") match {
      case s if s.isEmpty => default
      case x => x
    }
  }

}


object EmptyInput extends Input {
  override def toString: String = "Sure? "
}

class ProgramInput extends Input {
  val name = readUntilRight("Please enter your capitalized name", "Idiot")(_.charAt(0).isUpper)
}

object InputManagerUsage {
  def main(args: Array[String]) {
    val programInput = InputManager.readProgramInput()
    println(programInput)
  }
}