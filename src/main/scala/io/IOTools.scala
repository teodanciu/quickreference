package io

import java.io.{PrintWriter, File}
import scala.io.Source

object IOTools {

  trait IO[+A] {
    self =>
    def run: A

    def flatMap[B](f: A => IO[B]): IO[B] = IO(f(self.run).run)

    def map[B](f: A => B): IO[B] = IO(f(self.run))

    def andThen[B](f: IO[B]): IO[B] = IO {
      self.run
      f.run
    }
  }

  object IO {
    def unit[A](a: => A): IO[A] = IO(a)

    def apply[A](a: => A) = new IO[A] {
      def run = a
    }
  }

  def PrintLine(what: String): IO[Unit] = IO(println(what))

  def PrintToFile(fileName: String)(what: String) = IO {
    printToFile(new File(fileName)) {
      _.println(what)
    }

    def printToFile(f: File)(op: PrintWriter => Unit) = {
      val p = new PrintWriter(f)
      try op(p)
      finally p.close()
    }
  }

  def ReadLine: IO[String] = IO(readLine)

  def ReadFromFile(fileName: String): IO[Iterator[String]] = IO {
    Source.fromFile(new File(fileName)).getLines
  }
}


object IOToolsUsage {

  import IOTools._

  def readAndPrintToChannel(channel: String => IO[Unit]): IO[String] = {
    for {
      _ <- PrintLine("Please enter a word:")
      input <- ReadLine
      _ <- channel(input)
    } yield input
  }

  def main(args: Array[String]) {
    readAndPrintToChannel(PrintLine).run
    readAndPrintToChannel(PrintToFile("result")).run

  }

  def main2(args: Array[String]) {
    PrintLine("Hello! Please enter a word: ")
      .andThen(ReadLine)
      .map("You entered: " + _ + ". Magic!")
      .flatMap(PrintLine).run
  }

  def main3(args: Array[String]) {

    val discardIfStartsWithH: String => Option[String] = s => Some(s).find(!_.startsWith("h"))
    val discardIfContainsUpper: String => Option[String] = Some(_).find(_.forall(_.isLower))


    val action =
      for {
        _ <- PrintLine("Hello! Please enter a word. NO UPPER CASE CHARACTERS PLEASE!") //String
        input = ReadLine //Monad (not "unwrapped" = dereferenced
        sanitized <- input.map(discardIfContainsUpper) //  Option. monad has been mapped to a function that returns option AND THEN derefrenced
        result = sanitized.getOrElse("Malformed Expression") // result = sanitized.fold("Malformed expression")(identity)
        _ <- PrintLine(result)
      } yield ()

    //    action.run


    val equivalentAction =
      for {
        _ <- PrintLine("Hello! Please enter a word. NO UPPER CASE CHARACTERS PLEASE!") //String
        _ <- ReadLine map discardIfContainsUpper map (_.getOrElse("Malformed Expression")) flatMap PrintLine
      } yield ()

    equivalentAction.run

  }

}
