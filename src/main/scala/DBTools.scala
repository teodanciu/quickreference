import java.sql.{DriverManager, Connection}

object DBTools {

  //A Database Action: given a Connection, it can produce some value of type e
  case class DB[A](g: Connection => A) {
    def apply(c: Connection): A = g(c)

    //promote a function that operates on A and B to a function that operates on corresponding Database actions
    //we are a functor
    def map[B](f: A => B): DB[B] =
      DB[B] {
        c => f(g(c))
      }

    //combining to Actions
    //We have an action that produces a result of type A (this), and we take another action that CONSUMES that A (it depends on it) and produces a combined action
    //we are a monad
    def flatMap[B](f: A => DB[B]): DB[B] =
      DB[B] {
        c => f(g(c))(c)
      }
  }

  def pure[A](a: A): DB[A] = DB(c => a)

  trait ConnProvider {
    def apply[A](f: DB[A]): A
  }

  def makeProvider(driver: String, url: String) =
    new ConnProvider {
      def apply[A](databaseAction: DB[A]): A = {
        Class.forName(driver)
        val conn = DriverManager.getConnection(url)
        try {
          databaseAction(conn)
        } finally {
          conn.close
        }
      }
    }
}


object DBToolsUsageDaoImplementation {

  import DBTools._

  def setUserPwd(id: String, pwd: String): DB[Unit] = DB[Unit] {
    c =>
      val stmt = c.prepareStatement("update users set pwd =? where id = ?")
      stmt.setString(1, pwd)
      stmt.setString(2, id)
      stmt.executeUpdate()
  }

  def getUserPwd(userId: String): DB[String] = DB[String] {
    c =>
      val stmt = c.prepareStatement("select id from users where id = ?")
      stmt.setString(1, userId)
      stmt.executeQuery().getString("id")
  }

  def isPasswordEqual(userId: String, pwd: String): DB[Boolean] =
    getUserPwd(userId).map(_ == pwd)

  def changePwd(userId: String, oldPwd: String, newPwd: String): DB[Boolean] =
    for {
      pwd <- getUserPwd(userId)
      eq <- if (pwd == oldPwd) for {
        _ <- setUserPwd(userId, newPwd)
      } yield true
      else DBTools.pure(false)
    } yield eq


  def changePwd2(userId: String, oldPwd: String, newPwd: String): DB[Boolean] =
    for {
      pwd <- getUserPwd(userId)
      eq <- if (pwd == oldPwd)
        setUserPwd(userId, newPwd).map(_ => true)
      else DBTools.pure(false)
    } yield eq

  def changePwd3(userId: String, oldPwd: String, newPwd: String): DB[Boolean] =
    getUserPwd(userId).flatMap {
      pwd =>
        if (pwd == oldPwd)
          setUserPwd(userId, newPwd).map(_ => true)
        else DBTools.pure(false)
    }
}


object DBToolsUsageClient {

  import DBToolsUsageDaoImplementation._
  import DBTools._

  def changePassword(userId: String): ConnProvider => Unit =
    connectionProvider => {
      println("Enter old password")
      val oldPwd = readLine()
      println("Enter new password")
      val newPwd = readLine()
      connectionProvider {
        changePwd(userId, oldPwd, newPwd)
      }
    }

  def main(args: Array[String]) {
    lazy val mysqlProvider = makeProvider("org.ght.mm.mysqlDriver", "jdbc://mysql://prod:3306")
    lazy val sqliteTestDB = makeProvider("org.sqlite.JDBC", "jdbc:sqlite:memory:")
    lazy val hsqldb = makeProvider("org.hsqldb.jdbc.JDBCDriver", "jdbc:hsqldb:mem:test")

    changePassword("234")(hsqldb)
  }

}