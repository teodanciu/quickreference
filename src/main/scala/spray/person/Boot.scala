package spray.person

import akka.actor.{Props, ActorSystem}
import akka.actor.actorRef2Scala
import spray.can.Http
import akka.io.IO


object Boot extends App {
  // we need an ActorSystem to host our application in
  implicit val system = ActorSystem("S4")
  //
  //  // every spray-can HttpServer (and HttpClient) needs an IOBridge for low-level network IO
  //  // (but several servers and/or clients can share one)
  //  //val ioBridge = new IOBridge(system).start()
  //  private val ioBridge = IOExtension(system).ioBridge()
  //  // create and start our service actor
  //  val service = system.actorOf(Props[S4ServiceActor], "s4-service")
  //
  //  // create and start the spray-can HttpServer, telling it that
  //  // we want requests to be handled by our singleton service actor
  //  val httpServer = system.actorOf(
  //    Props(new HttpServer(ioBridge, SingletonHandler(service))),
  //    name = "http-server"
  //  )
  //
  //  // a running HttpServer can be bound, unbound and rebound
  //  // initially to need to tell it where to bind to
  //  httpServer ! HttpServer.Bind("localhost", 8080)
  //}

  val service = system.actorOf(Props[PersonServiceActor], "s4-service")

  IO(Http) ! Http.Bind(service, interface = "localhost", port = 8080)


}


// we need an ActorSystem to host our application in


// create and start our service actor
//  val service = system.actorOf(Props[CustomerServiceActor], "customer-service")

// start a new HTTP server on port 8080 with our service actor as the handler
//  IO(Http) ! Http.Bind(service, interface = "localhost", port = 8080)
