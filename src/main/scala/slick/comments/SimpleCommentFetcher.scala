package slick.comments

import com.typesafe.scalalogging.slf4j.Logging

import javax.sql.DataSource

import scala.slick.jdbc.{StaticQuery, GetResult}

import scala.slick.session.Database
import scala.slick.session.Session

import slick._

class SimpleCommentFetcher(val dataSource: DataSource) extends Logging {


  def fetchComments(articleId: String) = {

    val db = Database.forDataSource(dataSource)

    implicit val getUserCommentResult = GetResult(row =>
      UserComment(
        comment = Comment(row.nextInt, row.nextString, row.nextTimestamp, row.nextString, row.nextString, row.nextBoolean),
        user = User(row.nextInt, row.nextString, row.nextString, row.nextString, row.nextString)
      )
    )

    val query =
      """select comments.id, comments.body, comments.created_on, comments.target, comments.user_ip,
        not ((comment_categories.category <> 'post moderated spam' and comment_categories.category <> 'akismet spam') or comment_categories.category is  null) ,
        users.id, users.remote_id, users.first_name, users.last_name, users.email
        from comments inner join users on comments.user_id=users.id
        left join
        (moderations  inner join comment_categories on moderations.comment_category_id = comment_categories.id)
        on comments.id=moderations.comment_id where comments.target=? and not comments.hidden"""

    val commentsByDoi = StaticQuery.query[String, UserComment](query)

    val comments = db withSession {
      implicit session: Session =>
        commentsByDoi.list(articleId)
    }

    //the above list may contain many comments with the same id (depending on the number of rows in the moderations for that comment id)
    //The next line groups comments with the same id in a list
    val commentsById: Map[Int, List[UserComment]] = comments.groupBy(_.comment.id)

    //mark comment as spam if there exists one moderation of that comment (=one comment in the grouped list of comments) marking it as spam

    commentsById.map {
      case (commentId, comments) =>
        val firstComment = comments.head
        val moderationWithSpamExists = comments.exists(_.comment.spam)
        firstComment.setSpam(moderationWithSpamExists)
        firstComment
    }.toList

  }
}

