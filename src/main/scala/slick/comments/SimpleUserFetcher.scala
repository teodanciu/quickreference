package slick.comments

import javax.sql.DataSource
import java.lang.String
import scala.Predef.String
import com.typesafe.scalalogging.slf4j.Logging
import scala.slick.session.Database
import scala.slick.jdbc.{GetResult, StaticQuery}
import scala.collection.immutable.{Set, Map, ListMap}
import scala.slick.session.Session


class SimpleUserFetcher(val dataSource: DataSource) extends Logging {


  def tokenizedBannedUserDataFetchedJudiciously(excludedRemoteIds: Set[String], tokenizer: Tokenizer): Map[String, Option[User]] = {

    import tokenizer.{tokenizedEmail, tokenizedId}

    val db = Database.forDataSource(dataSource)
    val query = s"select id, remote_id, first_name, last_name, email, banned from users where banned"
    logger.debug(query)

    implicit val getUserResult: GetResult[(String, Option[User])] = GetResult {
      row =>
        val id = row.nextInt()
        val intRemoteId = row.nextInt()
        val dbRemoteId = String.valueOf(intRemoteId)
        val firstName = row.nextString()
        val lastName = row.nextString()
        val dbEmail = row.nextString()

        val tokenizedRemoteId = tokenizedId(dbRemoteId)

        if (excludedRemoteIds contains tokenizedRemoteId)
          (tokenizedRemoteId, None)
        else
          (tokenizedRemoteId, Some(User(id, tokenizedRemoteId, firstName, lastName, tokenizedEmail(dbEmail))))
    }

    val usersQuery = StaticQuery.queryNA(query)

    logger.debug("Executing query: " + usersQuery.getStatement + ".")

    val users: List[(String, Option[User])] = db withSession {
      implicit session: Session =>
        usersQuery.list
    }

    logger.debug("Number of Rows: " + users.size)
    ListMap(users: _*)
  }

}


class Tokenizer(token: String) extends Logging {
  def tokenizedEmail(email: String): String = if (email.contains("@"))
    email.replace("@", "@" + token)
  else {
    logger.error(s"Could not tokenize email: $email.")
    throw new RuntimeException
  }

  def tokenizedId(id: String): String = token + id
}
