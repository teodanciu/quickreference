package slick.comments

import java.sql.Timestamp

case class Comment(id: Int,
                   var body: String,
                   creationDate: Timestamp,
                   articleId: String,
                   userIp: String,
                   var spam: Boolean) {

  def approved: Boolean = !spam

  def setBody(body: String) {
    this.body = body
  }

}

case class UserComment(var comment: Comment, user: User) {
  def setComment(comment: Comment) {
    this.comment = comment
  }

  def setSpam(spam: Boolean) {
    this.comment.spam = spam
  }
}


case class User(id: Int, remoteId: String, firstName: String, lastName: String, email: String)

case class TimeInterval(startDate: Long, endDate: Long)
