package xml

import scala.xml.{XML, Node, MetaData, NodeSeq}
import java.io.Reader

class Elements(xml: NodeSeq) {

  private def attributeBasedPredicate(att: MetaData => Boolean): Node => Boolean =
    node => node.attributes.exists(att)

  def withTag(name: String): Elements =
    new Elements(xml \\ name)

  def withAttributeEqualTo(name: String)(value: String): Elements = {
    withAttribute(name)(value)(_ == _)
  }

  def withAttribute(name: String)(value: String)(relation: (String, String) => Boolean) = {
    val nodeSeq = xml filter attributeBasedPredicate(att => att.key == name && relation(att.value.text, value))
    new Elements(nodeSeq)
  }

  def withAttributeClass(value: String): Elements =
    withAttribute("class")(value)(_.contains(_))

  def textOfFirst(n: Int): List[String] =
    xml.take(n).map(_.text).toList

  def textOfFirst: String =
    xml.take(1).map(_.text).toList(0)

  def toNodeSeq: NodeSeq = xml

  override def toString = xml.toString
}

object Elements {
  def of(xml: Node): Elements =
    new Elements(xml)
}

object ElementsUsage {
  def parseWeekSchedule(reader: Reader): Map[String, List[(String, String)]] = {
    val wholeXml = XML.load(reader)

    val weekContainer = Elements.of(wholeXml).withTag("div").withAttributeClass("ff-daily-gx-timetable-container")
    val dayNodes = weekContainer.withTag("div").withAttributeClass("ff-tab-panel").toNodeSeq
    (for {
      dayNode <- dayNodes
      day: String = Elements.of(dayNode).withTag("p").withAttributeClass("ff-tab-title").textOfFirst
      activityNodes: NodeSeq = Elements.of(dayNode).withTag("table").withAttributeClass("ff-timetable").withTag("tbody").withTag("tr").toNodeSeq
      dayActivities: List[(String, String)] = parseActivities(activityNodes)
    } yield (day, dayActivities)).toMap
  }

  private def parseActivities(activityNodes: NodeSeq): List[(String, String)] = {
    activityNodes.map {
      node =>
        val List(interval, activity) = Elements.of(node).withTag("td").textOfFirst(2)
        (interval, activity)
    }.toList
  }
}