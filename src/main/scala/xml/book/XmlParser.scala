package xml.book

object XmlParser {

  case class Song(val title: String, val length: String) {
    lazy val time = {
      val Array(minutes, seconds) = length.split(":")
      minutes.toInt * 60 + seconds.toInt
    }
  }

  case class Album(val title: String, val songs: Seq[Song], val description: String) {
    lazy val time = songs.map(_.time).sum
    lazy val length = (time / 60) + ":" + (time % 60)
  }

  case class Artist(val name: String, val albums: Seq[Album])

  val musicElem = scala.xml.XML.loadFile("/tmp/music.xml")

  val songs = (musicElem \\ "song").map {
    song =>
      Song((song \ "@title").text, (song \ "@length").text)
  }

  val artists = (musicElem \ "artist").map {
    artist =>
      val name = (artist \ "@name").text
      val albums = (artist \ "album").map {
        album =>
          val title = (album \ "@title").text
          val description = (album \ "description").text
          val songList = (album \ "song").map {
            song =>
              Song((song \ "@title").text, (song \ "@length").text)
          }
          Album(title, songList, description)
      }
      Artist(name, albums)

  }

  val marshalled =
    <music>
      {artists.map {
      artist =>
        <artist name={artist.name}>
          {artist.albums.map {
          album =>
            <album title={album.title}>
              {album.songs.map(song => <song title={song.title} length={song.length}/>)}<description>
              {album.description}
            </description>
            </album>
        }}
        </artist>
    }}
    </music>


}
